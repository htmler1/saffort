$(document).ready(function () {
	
	var badgesSlider = new BadgesSlider(2000);

	nonDesktopSearch();
	
	iniCartPopover();

	svgFallback();
	
	initTabsForMobile();
	
	qualityBadgesSlider();

	var tabSlides = new TabSlides($(".nav-tabs"));
	
	// attributes filter
	function toggleAttributesFilter(item) {
		$(item).parent('.attribute-filter').toggleClass('closed');
	}
	$('body').on('click', '.listing-filter .attribute-filter .attribute-name', function () {
		toggleAttributesFilter(this);
	});
	$('body').on('click', '#sidr-left .attribute-filter .attribute-name', function () {
		toggleAttributesFilter(this);
	});

	initSidr();
	
	initCompareSticker();

	initTextPageMenus();
	
});
