// tabs in mobile
// Priklausomas nuo overlay.js
var initTabsForMobile = function () {
	var body = $('body'),
		togglerSelector = '.mobile-tabs-toggler',
		overlay = initOverlay(),
		fixWidth = function (toggler) {
			var navigation = toggler.next('.nav');
			if (toggler.is(':visible')) {
				navigation.css('width', toggler.css('width'));
			} else {
				navigation.css('width', 'auto');
			}
		};
	
	body.on('click', togglerSelector, function () {
		var toggler = $(this);
		toggler.toggleClass('active');
		overlay.toggleClass('active');
		fixWidth(toggler);
	});

	$(window).bind('orientationchange', function() {
		$(togglerSelector).each(function () {
			fixWidth($(this));
		});
	});

	$(window).smartresize(function() {
		$(togglerSelector).each(function () {
			fixWidth($(this));
		});
	});
};