function iniCartPopover () {
	// Pagal klase shopping-cart-empty nusprendziam ar cartas tuscias, ar ne
	function isCartEmpty() {
		return $('.shopping-cart').hasClass('.shopping-cart-empty');
	}
	
	// extend tooltip - copy of tooltip
	$.fn.popover.Constructor.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
		var $cartReturn;
		if (placement == 'bottom bottom-left') {
			$cartReturn = { top: pos.top + pos.height + 7,   left: pos.left + pos.width / 2 - 25  };
			if (!isCartEmpty()) {
				$cartReturn = { top: pos.top + pos.height + 7, left: pos.left  -actualWidth + $('.shopping-cart').width() };
			}
		}
		return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2  } :
				placement == 'bottom bottom-left' ? $cartReturn :
				placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2  } :
			   placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
			/* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width   }
	};
	
	var popoverTrigger = $('.products-in-cart'),
		popoverContent = popoverTrigger.next('.popover-content-container');
	
	popoverTrigger.popover({
		placement: 'bottom bottom-left', // extended bootstrap
		trigger: 'manual',
		html: true,
		content: function () {
			return popoverContent.html();
		}
	});
	
	// in desktop disabled 
	popoverTrigger.click(function() {
		if (!isDesktop()) {
			$(this).popover('toggle');
		}
	});
	
	$('.shopping-cart').hover(function () {
		if (isDesktop()) {
			if (isCartEmpty()) {
				
			} else {
				
			}
			popoverTrigger.popover('show');
		}
	}, function () {
		if (isDesktop()) {
			popoverTrigger.popover('hide');
		}
	});
	
	// on resize - hide {
	$(window).smartresize(function() {
		popoverTrigger.popover('hide');
	});
}