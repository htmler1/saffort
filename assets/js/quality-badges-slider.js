function qualityBadgesSlider() {
	var jcarousel = $('.quality-badges');

	jcarousel.on('jcarousel:reload jcarousel:create', function () {
			var width = jcarousel.innerWidth();

			if (width >= 1180) {
				width = width / 5;
			} else if (width >= 710) {
				width = width / 3;
			}

			jcarousel.jcarousel('items').css('width', width + 'px');
		})
		.jcarousel({
			wrap: 'circular'
		});

	$('.jcarousel-control-prev').jcarouselControl({
		target: '-=1'
	});

	$('.jcarousel-control-next').jcarouselControl({
		target: '+=1'
	});
}