function initSidr () {
	$('.mobile-menu-toggle').sidr({
		name: 'sidr-right',
		source: '#topMenu',
		side: 'right',
		renaming: false
	});
	
	// on resize - close {
	$(window).smartresize(function() {
		$.sidr('close', 'sidr-right');
		$.sidr('close', 'sidr-left');
		$('.mobile-menu-toggle').removeClass('active');
		$('.filter-sticky').removeClass('active');
	});
	
	$('.mobile-menu-toggle').click(function () {
		$(this).toggleClass('active');
	});
	
	$('.filter-sticky').click(function () {
		$(this).toggleClass('active');
	});
	
	// FILTER
	$('.filter-sticky').sidr({
		name: 'sidr-left',
		source: '.listing-filter',
		side: 'left',
		renaming: false
	});
}