
<div class="shopping-cart shopping-cart-non-empty">
	<a href="#" class="products-in-cart"><span>5</span></a>
	<div class="hidden popover-content-container">
		<h2>BASKET</h2>
		<table>
			<tr>
				<td class="basket-product-picture">
					<img src="media/compare1.png" />
				</td>
				<td>
					<strong>MOTTURA CASH 11.C</strong>
					<span class="basket-product-price">&pound;519.99 <span class="vat">ex. VAT</span></span>
					<br />
					Quantity: <span class="qty">2</span>
				</td>
				<td>
					<a href="#" class="remove-product-icon"></a>
				</td>
			</tr>
			<tr>
				<td>
					<img src="media/compare2.png" />
				</td>
				<td>
					<strong>MOTTURA CASH 11.C</strong>
					<span class="basket-product-price">&pound;1749.99 <span class="vat">ex. VAT</span></span>
					<br />
					Quantity: <span class="qty">3</span>
				</td>
				<td>
					<a href="#" class="remove-product-icon"></a>
				</td>
			</tr>
		</table>
		<p>
			Total:
			<span class="cart-price">&pound;27,999.95</span>
		</p>
		<a href="#" class="btn btn-success btn-lg">GO TO CHECKOUT</a>
	</div>
	<span class="cart-name">Basket</span>
	<span class="total-price">&pound;0</span>
	<a href="#" class="login-link">Login</a>
</div>