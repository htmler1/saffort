/**
 * javascript responsive helper 
 * 
 * in HTML is one element .scrollbar-measure, that in CSS has width in
 *  30px, if Mobile mode activ,
 *  20px, if Tablet and
 *  10px, if Desktop
 */

var respHelper = undefined,
	respHelperClass = ".scrollbar-measure";

function initRespHelper () {
	if (respHelper === undefined) {
		respHelper = $(respHelperClass);
	}
}

function isMobile () {
	initRespHelper();
	return respHelper.width() >= 30;
}

function isTablet () {
	initRespHelper();
	return respHelper.width() >= 20 && respHelper.width() < 30;
}

function isDesktop () {
	initRespHelper();
	return respHelper.width() >= 10 && respHelper.width() < 20;
}