/**
 * bottom line slides to active element on activity change
 * 
 * http://css-tricks.com/jquery-magicline-navigation/
 */
var TabSlides = function ($mainNav) {

	var $el, leftPos, newWidth;
	
    $mainNav.append("<li class='magic-line'><i></i></li>");
    var $magicLine = $mainNav.find(".magic-line");
	
	var slideLine = function ($el) {
		if ($el.length === 0) {
			return;
		}
		leftPos = $el.parent().position().left + $el.position().left;
		newWidth = $el.parent().width();
		$magicLine.stop().animate({
			left: leftPos,
			width: newWidth
		});
	};
	
	$mainNav.find('a').click(function (e) {
		e.preventDefault();
		$el = $(this);

		// for other devs: here are tabs initialisiert
		$el.tab('show');
		slideLine($el);
	});
    
	slideLine($mainNav.find(".active a"));
	
};