var initOverlay = function () {
	if ($('.mobile-tabs-overlay').length === 0) {
		var overlay = $('<div class="mobile-tabs-overlay"></div>');

		$('body').append(overlay);
		
	}
	
	return $('.mobile-tabs-overlay');
};