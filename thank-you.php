
<!DOCTYPE html><!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="lt"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="lt"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="lt"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="lt"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Saffort</title>
		
		<link href="assets/css/bootstrap.css" rel="stylesheet" />
		<link href="assets/css/styles.css" rel="stylesheet" />
		<link href="assets/animate.css/animate.css" rel="stylesheet" />
		
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		
		<!--[if lt IE 9]>
		<script src="assets/respond.js/respond.js"></script>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>		
		<header id="header">
	<div class="container-fluid">

		<a href="start.php" id="logo">
			<img src="assets/img/logos/logo.png" alt="saffort logo" class="svgFallback" />
		</a>

		<div class="safes-available">
			<p class="heading">
				Every Safe in One Place
			</p>
			<p>
				<span class="total">
					1483
				</span>Safes Available
			</p>
		</div>

		
<div class="shopping-cart shopping-cart-non-empty">
	<a href="#" class="products-in-cart"><span>5</span></a>
	<div class="hidden popover-content-container">
		<h2>BASKET</h2>
		<table>
			<tr>
				<td class="basket-product-picture">
					<img src="media/compare1.png" />
				</td>
				<td>
					<strong>MOTTURA CASH 11.C</strong>
					<span class="basket-product-price">&pound;519.99 <span class="vat">ex. VAT</span></span>
					<br />
					Quantity: <span class="qty">2</span>
				</td>
				<td>
					<a href="#" class="remove-product-icon"></a>
				</td>
			</tr>
			<tr>
				<td>
					<img src="media/compare2.png" />
				</td>
				<td>
					<strong>MOTTURA CASH 11.C</strong>
					<span class="basket-product-price">&pound;1749.99 <span class="vat">ex. VAT</span></span>
					<br />
					Quantity: <span class="qty">3</span>
				</td>
				<td>
					<a href="#" class="remove-product-icon"></a>
				</td>
			</tr>
		</table>
		<p>
			Total:
			<span class="cart-price">&pound;27,999.95</span>
		</p>
		<a href="#" class="btn btn-success btn-lg">GO TO CHECKOUT</a>
	</div>
	<span class="cart-name">Basket</span>
	<span class="total-price">&pound;0</span>
	<a href="#" class="login-link">Login</a>
</div>
		<div class="language-selector dropdown">
			<a data-toggle="dropdown" href="#">EN</a>
			<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				<li><a href="#">LT</a></li>
				<li><a href="#">RU</a></li>
			</ul>
		</div>

		<form action="" method="post" class="search-form">
			<input type="text" placeholder="Search for products or entire website" name="_search" value="" />
			<span class="close-search close">&times;</span>
			<button type="submit"><i class="glyphicon glyphicon-search"></i></button>
		</form>

		<div class="header-contacts">
			<span class="phone"><i class="glyphicon glyphicon-earphone"></i> 01543 268631</span>
			<a class="email" href="mailto:info@saffort.co.uk">info@saffort.co.uk</a>
		</div>

		<div class="badges-slider">
			<div class="slider-item badges-icon-label">
				Lowest price warranty
			</div>
			<div class="slider-item badges-icon-truck">
				Free Delivery
			</div>
		</div>

		<nav id="topMenu">
			
			<div class="mobile-menu-toggle">
				<span>Categories</span>
				<span class="icon">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</div>
			
			<ul class="nav nav-pills">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						All Safes
						<span class="caret"></span>
					</a>
					<div class="dropdown-menu">
						<div class="dropdown-categorie-listing">
							<h3>Most popular categories</h3>
							<ul>
								<li><a href="#">Gun Safes <span class="total-count">(25)</span></a></li>
								<li><a href="listing.php">Security Safes <span class="total-count">(43)</span></a></li>
								<li><a href="#">Fire Safes <span class="total-count">(32)</span></a></li>
								<li><a href="#">Wall & Underfloor Safes <span class="total-count">(15)</span></a></li>
								<li><a href="#">Home Safes <span class="total-count">(28)</span></a></li>
							</ul>
						</div>
						<div class="dropdown-categorie-listing">
							<h3>Other categories</h3>
							<ul>
								<li><a href="#">Vivamus Safes <span class="total-count">(41)</span></a></li>
								<li><a href="#">Lorem Safes <span class="total-count">(23)</span></a></li>
								<li><a href="#">Euismod nequri Safes <span class="total-count">(14)</span></a></li>
								<li><a href="#">Etiam Safes <span class="total-count">(12)</span></a></li>
								<li><a href="#">Suspendisse Safes <span class="total-count">(13)</span></a></li>
							</ul>
							<ul>
								<li><a href="#">Sollicitudin Safes <span class="total-count">(18)</span></a></li>
								<li><a href="#">Ipsum Safes <span class="total-count">(11)</span></a></li>
								<li><a href="#">Nam interdum Safes <span class="total-count">(7)</span></a></li>
							</ul>
						</div>
						<ul>
							<li>
								<a href="#">View All Safes <span class="total-count">(1483)</span></a>
							</li>
						</ul>
					</div>
				</li>
				<li>
					<a href="#">
						Gun Safes <span class="total-count">(25)</span>
					</a>
				</li>
				<li class="active">
					<a href="listing.php">
						Security Safes <span class="total-count">(33)</span>
					</a>
				</li>
				<li>
					<a href="#">Fire Safes <span class="total-count">(22)</span>
					</a>
				</li>
				<li>
					<a href="#">Wall & Underfloor Safes <span class="total-count">(25)</span>
					</a>
				</li>
				<li>
					<a href="#">Home Safes <span class="total-count">(5)</span>
					</a>
				</li>
				<li class="highlighted"><a href="#">Special Offers</a></li>
			</ul>
		</nav>

	</div>
</header>		
		<div class="container-fluid">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="block-for-text fill-xs thank-you-page">
						
						<div class="row">
							
							<div class="col-sm-5 hidden-xs">
								<img src="assets/img/shopping-cart.png" class="pull-right" />
							</div>
							
							<div class="col-xs-12 col-sm-7">
								<h2>Your order has been successful!</h2>
								<p>Thank you for choosing Saffort.<br />
									The order details has been sent to <strong>tadas.klimavicius@gmail.com</strong></p>
								<p><a href="#" class="continue">Continue shopping</a></p>
							</div>
							
						</div>
						
					</div>
				</div>
				
			</div>
			
		</div>
		
		<footer id="footer">
	<div class="footer-top">
		<div class="container-fluid">
			<div class="quality-badges">
				<ul>
					<li>
						<a class="quality-badge quality-badge-icon-truck" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Free Delivery</strong>
								<span class="quality-badge-text">Free UK delivery on all orders</span>
							</span>
						</a>
					</li>
					<li>
						<a class="quality-badge quality-badge-icon-refresh" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Free Returns</strong>
								<span class="quality-badge-text">We accept returns if you are not satisfied</span>
							</span>
						</a>
					</li>
					<li>
						<a class="quality-badge quality-badge-icon-label" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Lowest price warranty</strong>
								<span class="quality-badge-text">We guarantee the best price online.</span>
							</span>
						</a>
					</li>
					<li>
						<a class="quality-badge quality-badge-icon-lock" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Safe to buy</strong>
								<span class="quality-badge-text">Be sure to stay safe online shopping with us</span>
							</span>
						</a>
					</li>
					<li>
						<a class="quality-badge quality-badge-icon-headphone" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Professional advice</strong>
								<span class="quality-badge-text">Feel free to contact us with any question, we'll help you</span>
							</span>
						</a>
					</li>
				</ul>
			</div>
			<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
			<a href="#" class="jcarousel-control-next">&rsaquo;</a>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row footer-menu-blocks">
		
			<div class="col-xs-6 col-sm-6 col-lg-3 footer-menu-block">
				
				<h4>Saffort Store</h4>

				<ul class="url-list">
					<li><a href="#">About us</a></li>
					<li><a href="#">Why Saffort?</a></li>
					<li><a href="#">News and Updates</a></li>
					<li><a href="#">Contacts</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-6 col-lg-3 footer-menu-block">
				<h4>Products</h4>

				<ul class="url-list">
					<li><a href="#">View all products</a></li>
					<li><a href="#">Shop by brands</a></li>
					<li><a href="#">Shop by categories</a></li>
					<li><a href="#">Saffort Wizard</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-lg-3 footer-menu-block">

				<h4>Customer Support</h4>

				<ul class="url-list">
					<li><a href="#">Definitions A-Z</a></li>
					<li><a href="#">F.A.Q</a></li>
					<li><a href="#">Experts advice</a></li>
					<li><a href="#">Downloads</a></li>
				</ul>

			</div>
			<div class="col-xs-6 col-lg-3 footer-menu-block">
				<h4>Website Info</h4>

				<ul class="url-list">
					<li><a href="#">Payments and Delivery</a></li>
					<li><a href="#">Website Security</a></li>
					<li><a href="#">Terms and Conditions</a></li>
					<li><a href="#">Returns</a></li>
				</ul>
			</div>
		</div>

		<p>SAFFORT � tai did�iausias prekybos seifais tinklas. Musu tikslas � 
			sudaryti Jums galimybe vienoje vietoje rasti visus seifus. Todel pas 
			mus parduodami: buitiniai, prie�gaisriniai, sertifikuoti, �arvuoti, 
			seifai ginklams, sieniniai seifai ir t.t., kuriu kokybe mes neabejojame 
			ir kuriems galime u�tikrinti nepriekai�tinga pristatyma ir servisa. 
			Visa tai - su geriausios kainos garantija ir nemokamu pristatymu Lietuvoje.
			Kartu siekiame, kad seifu pasirinkimo procesas butu paprastas ir malonus.
			I�sirinkti seifa Jums nereikes specialiu �iniu. Kad ir koks butu Jusu
			norimas seifas, pas mus Jus turite galimybe ji lengvai i�sirinkti 
			pasinaudodami seifu atrankos gidu, neribotomis palyginimo galimybemis,
			filtravimu, kuri naudojant keliais peles paspaudimais Jus galite i� 
			�imtu seifu surasti norima pagal dydi, svori, seifo spynos tipa, kaina,
			seifo gamintoja bei kitus parametrus. I�sirinkta seifa galite pamatyti 
			SAFFORT prekybos vietose � ekspozicijoje Vilniuje daugiau nei 100 seifu.
			Norime nuolatos tobuleti ir nenustebkite, kad kita karta apsilanke SAFFORT
			puslapyje rasite daugiau seifu, dar supaprastinta atranka ar kitu naujoviu.
			Labai vertiname Jusu pastebejimus, tad pra�ome ra�yti atsiliepimus apie 
			puslapi, apie parduodamus seifus, apie musu servisa � reaguosime ir tobulesime.</p>
		
	</div>

	<div class="country-selector">
		<ul>
			<li><a href="#">Lithuania</a></li>
			<li><a href="#">Latvia</a></li>
			<li><a href="#">United Kingdom</a></li>
			<li><a href="#">Germany</a></li>
			<li><a href="#">France</a></li>
			<li><a href="#">Netherlands</a></li>
			<li><a href="#">Poland</a></li>
		</ul>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-lg-8 copyright">
				<img src="assets/img/logos/logo-horizontal.png" alt="" class="logo-horizontal svgFallback" />
				<p>
					&COPY; Dremler Ltd. 2013. All rights reserved. <br />
					VAT number: 742 2441 58. saffort.co.uk is a trading name of Dremler Ltd.
				</p>
			</div>
			<div class="col-xs-12 col-sm-8 col-lg-4">
				<ul class="bank-logos">
					<li><a href="#" class="bank-logo visa-logo">Visa</a></li>
					<li><a href="#" class="bank-logo master-logo">Master</a></li>
					<li><a href="#" class="bank-logo maestro-logo">Maestro</a></li>
					<li><a href="#" class="bank-logo discover-logo">Discover</a></li>
					<li><a href="#" class="bank-logo paypal-logo">Paypal</a></li>
				</ul>
			</div>
			<a href="#" class="idea-logo">IDEA Design</a>
		</div>
	</div>
</footer>

<script src="assets/js/vendor/modernizr.custom.83451.js"></script>
<script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
<script src="assets/bootstrap-3.1.1/dist/js/bootstrap.js"></script>
<script src="assets/js/vendor/jquery.jcarousel.min.js "></script>
<script src="assets/sidr-package-1.2.1/jquery.sidr.min.js"></script>
<script src="assets/js/smartresize.js"></script>
<script src="assets/js/responsive-helper.js"></script>
<script src="assets/js/badges.js"></script>
<script src="assets/js/cart-popover.js"></script>
<script src="assets/js/non-desktop-search.js"></script>
<script src="assets/js/tab-slides.js"></script>
<script src="assets/js/tab-mobile.js"></script>
<script src="assets/js/quality-badges-slider.js"></script>
<script src="assets/js/svg-fallback.js"></script>
<script src="assets/js/text-page-menu.js"></script>
<script src="assets/js/sidr.js"></script>
<script src="assets/js/compare.js"></script>
<script src="assets/js/overlay.js"></script>

<script src="assets/js/script.js"></script>

<div class="scrollbar-measure"></div>		
	</body>
</html>
