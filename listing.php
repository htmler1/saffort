
<!DOCTYPE html><!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="lt"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="lt"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="lt"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="lt"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Saffort</title>
		
		<link href="assets/css/bootstrap.css" rel="stylesheet" />
		<link href="assets/css/styles.css" rel="stylesheet" />
		<link href="assets/animate.css/animate.css" rel="stylesheet" />
		
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		
		<!--[if lt IE 9]>
		<script src="assets/respond.js/respond.js"></script>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>		
		<header id="header">
	<div class="container-fluid">

		<a href="start.php" id="logo">
			<img src="assets/img/logos/logo.png" alt="saffort logo" class="svgFallback" />
		</a>

		<div class="safes-available">
			<p class="heading">
				Every Safe in One Place
			</p>
			<p>
				<span class="total">
					1483
				</span>Safes Available
			</p>
		</div>

		
<div class="shopping-cart shopping-cart-non-empty">
	<a href="#" class="products-in-cart"><span>5</span></a>
	<div class="hidden popover-content-container">
		<h2>BASKET</h2>
		<table>
			<tr>
				<td class="basket-product-picture">
					<img src="media/compare1.png" />
				</td>
				<td>
					<strong>MOTTURA CASH 11.C</strong>
					<span class="basket-product-price">&pound;519.99 <span class="vat">ex. VAT</span></span>
					<br />
					Quantity: <span class="qty">2</span>
				</td>
				<td>
					<a href="#" class="remove-product-icon"></a>
				</td>
			</tr>
			<tr>
				<td>
					<img src="media/compare2.png" />
				</td>
				<td>
					<strong>MOTTURA CASH 11.C</strong>
					<span class="basket-product-price">&pound;1749.99 <span class="vat">ex. VAT</span></span>
					<br />
					Quantity: <span class="qty">3</span>
				</td>
				<td>
					<a href="#" class="remove-product-icon"></a>
				</td>
			</tr>
		</table>
		<p>
			Total:
			<span class="cart-price">&pound;27,999.95</span>
		</p>
		<a href="#" class="btn btn-success btn-lg">GO TO CHECKOUT</a>
	</div>
	<span class="cart-name">Basket</span>
	<span class="total-price">&pound;0</span>
	<a href="#" class="login-link">Login</a>
</div>
		<div class="language-selector dropdown">
			<a data-toggle="dropdown" href="#">EN</a>
			<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				<li><a href="#">LT</a></li>
				<li><a href="#">RU</a></li>
			</ul>
		</div>

		<form action="" method="post" class="search-form">
			<input type="text" placeholder="Search for products or entire website" name="_search" value="" />
			<span class="close-search close">&times;</span>
			<button type="submit"><i class="glyphicon glyphicon-search"></i></button>
		</form>

		<div class="header-contacts">
			<span class="phone"><i class="glyphicon glyphicon-earphone"></i> 01543 268631</span>
			<a class="email" href="mailto:info@saffort.co.uk">info@saffort.co.uk</a>
		</div>

		<div class="badges-slider">
			<div class="slider-item badges-icon-label">
				Lowest price warranty
			</div>
			<div class="slider-item badges-icon-truck">
				Free Delivery
			</div>
		</div>

		<nav id="topMenu">
			
			<div class="mobile-menu-toggle">
				<span>Categories</span>
				<span class="icon">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</div>
			
			<ul class="nav nav-pills">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						All Safes
						<span class="caret"></span>
					</a>
					<div class="dropdown-menu">
						<div class="dropdown-categorie-listing">
							<h3>Most popular categories</h3>
							<ul>
								<li><a href="#">Gun Safes <span class="total-count">(25)</span></a></li>
								<li><a href="listing.php">Security Safes <span class="total-count">(43)</span></a></li>
								<li><a href="#">Fire Safes <span class="total-count">(32)</span></a></li>
								<li><a href="#">Wall & Underfloor Safes <span class="total-count">(15)</span></a></li>
								<li><a href="#">Home Safes <span class="total-count">(28)</span></a></li>
							</ul>
						</div>
						<div class="dropdown-categorie-listing">
							<h3>Other categories</h3>
							<ul>
								<li><a href="#">Vivamus Safes <span class="total-count">(41)</span></a></li>
								<li><a href="#">Lorem Safes <span class="total-count">(23)</span></a></li>
								<li><a href="#">Euismod nequri Safes <span class="total-count">(14)</span></a></li>
								<li><a href="#">Etiam Safes <span class="total-count">(12)</span></a></li>
								<li><a href="#">Suspendisse Safes <span class="total-count">(13)</span></a></li>
							</ul>
							<ul>
								<li><a href="#">Sollicitudin Safes <span class="total-count">(18)</span></a></li>
								<li><a href="#">Ipsum Safes <span class="total-count">(11)</span></a></li>
								<li><a href="#">Nam interdum Safes <span class="total-count">(7)</span></a></li>
							</ul>
						</div>
						<ul>
							<li>
								<a href="#">View All Safes <span class="total-count">(1483)</span></a>
							</li>
						</ul>
					</div>
				</li>
				<li>
					<a href="#">
						Gun Safes <span class="total-count">(25)</span>
					</a>
				</li>
				<li class="active">
					<a href="listing.php">
						Security Safes <span class="total-count">(33)</span>
					</a>
				</li>
				<li>
					<a href="#">Fire Safes <span class="total-count">(22)</span>
					</a>
				</li>
				<li>
					<a href="#">Wall & Underfloor Safes <span class="total-count">(25)</span>
					</a>
				</li>
				<li>
					<a href="#">Home Safes <span class="total-count">(5)</span>
					</a>
				</li>
				<li class="highlighted"><a href="#">Special Offers</a></li>
			</ul>
		</nav>

	</div>
</header>		
		<div class="container-fluid">
			
			<div class="row visible-lg">
				<div class="col-xs-6">
					<ol class="breadcrumb">
						<li><a href="#"><i class="glyphicon glyphicon-home"></i>Home</a></li>
						<li class="active">Security Safes</li>
					</ol>
				</div>
				
			</div>
			
			<div class="row listing-description-and-settings">
				<div class="col-xs-12">
					<div class="listing-description">
						<div class="row">
							<div class="col-sm-7 col-lg-9">
								<h1>Security Safes</h1>
								<p class="visible-lg">
									Saffort are proud to stock over 200 home safes which are ideal for use in the home or office and suitable for protecting cash and valuables.
									Our Home Safes models come with either single key, electronic, combination locks or the latest biometric identification technology.
								</p>
							</div>

							<div class="hidden-xs col-sm-5 col-lg-3">
								<div class="block-wizzard smaller-wizzard clearfix">
	<p class="block-wizzard-title">New to safes?</p>
	<p>Choose your perfect safe in 4 easy steps</p>
	<a href="#" class="btn">Open SAFFORT Wizzard</a>
	<a href="#" class="hidden-link-hack">Open SAFFORT Wizzard</a>
	<!--We need it for link on mobile and desktop. in tablet is it hidden-->
</div>							</div>
						</div>
					</div>
				</div>
				
				<div class="hidden-xs col-lg-6 listing-sorting-settings">
					Showing 24 of 134 items sorted by
					<div class="sorter-selector dropdown select-like">
						<a data-toggle="dropdown" href="#">Lowest Price</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<li><a href="#">Biggest Price</a></li>
							<li><a href="#">Something</a></li>
						</ul>
					</div>
					and
					<div class="pagination-size-selector dropdown select-like">
						<a data-toggle="dropdown" href="#">15</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<li><a href="#">30</a></li>
							<li><a href="#">45</a></li>
						</ul>
					</div>
					items per page
				</div>
				
				<div class="visible-xs col-xs-12 listing-sorting-settings">
					Sorted by
					<div class="sorter-selector dropdown select-like">
						<a data-toggle="dropdown" href="#">Lowest Price</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<li><a href="#">Biggest Price</a></li>
							<li><a href="#">Something</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="compare-sticky sticky hidden-xs">
				Compare
			</div>
			<div class="compare-list">
				<ul>
					<li>
						<a href="#" class="compareproduct"><img src="media/compare1.png" /></a>
						<a href="#"class="compare-product-close"></a>
					</li>
					<li>
						<a href="#" class="compareproduct"><img src="media/compare2.png" /></a>
						<a href="#"class="compare-product-close"></a>
					</li>
					<li class="empty">Select item to compare</li>
				</ul>
				<a href="#" class="btn btn-warning btn-block">Compare</a>
			</div>
			
			<div class="row">
				<div class="col-lg-3">
					<div class="filter-sticky sticky hidden-lg">
						Filter
					</div>
					<div class="listing-filter">
	<div class="attribute-filter">
		<p class="attribute-name">Cash rating</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						&pound;1000
						<span class="total-count">(5)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" />
					<span>
						&pound;2000
						<span class="total-count">(5)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" />
					<span>
						&pound;3000
						<span class="total-count">(5)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked" />
					<span>
						&pound;3000
						<span class="total-count">(5)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked" />
					<span>
						&pound;3000
						<span class="total-count">(5)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" />
					<span>
						&pound;3000
						<span class="total-count">(5)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" />
					<span>
						&pound;3000
						<span class="total-count">(5)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" />
					<span>
						&pound;100 000
						<span class="total-count">(4)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter">
		<p class="attribute-name">Fire rating</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						30 minutes
						<span class="total-count">(15)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked" />
					<span>
						60 minutes
						<span class="total-count">(7)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked"  />
					<span>
						90 minutes
						<span class="total-count">(3)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" />
					<span>
						120+ minutes
						<span class="total-count">(8)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter">
		<p class="attribute-name">Lock type</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox"  />
					<span>
						Key
						<span class="total-count">(8)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox"  />
					<span>
						Fingerprint
						<span class="total-count">(3)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked"  />
					<span>
						Multiple locks
						<span class="total-count">(7)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked" />
					<span>
						Dial combination 
						<span class="total-count">(9)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter">
		<p class="attribute-name">Price</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox"  />
					<span>
						Key
						<span class="total-count">(8)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox"  />
					<span>
						Fingerprint
						<span class="total-count">(3)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked"  />
					<span>
						Multiple locks
						<span class="total-count">(7)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked" />
					<span>
						Dial combination 
						<span class="total-count">(9)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter">
		<p class="attribute-name">Delivery time</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox"  />
					<span>
						Key
						<span class="total-count">(8)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox"  />
					<span>
						Fingerprint
						<span class="total-count">(3)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked"  />
					<span>
						Multiple locks
						<span class="total-count">(7)</span>
					</span>
				</label>
			</li>
			<li>
				<label><input type="checkbox" checked="checked" />
					<span>
						Dial combination 
						<span class="total-count">(9)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	
	<div class="attribute-filter closed">
		<p class="attribute-name">Height</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter closed">
		<p class="attribute-name">Width</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter closed">
		<p class="attribute-name">Depth</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter closed">
		<p class="attribute-name">Capacity</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter closed">
		<p class="attribute-name">Weight</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
	<div class="attribute-filter closed">
		<p class="attribute-name">Brand</p>
		<ul>
			<li>
				<label><input type="checkbox" />
					<span>
						Electronic keypad
						<span class="total-count">(14)</span>
					</span>
				</label>
			</li>
		</ul>
	</div>
</div>				</div>
				
				<div class="col-xs-12 col-lg-9">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="product-listing-item product-offer-label">
								<a href="#" class="product-main-link">
									<img src="media/prod1.png" alt="" />
									<span class="product-title">MOTTURA CASH 11.C 132 N</span>
								</a>

								<div class="product-prices">
									<del>&pound;1399.99</del> Save &pound;140.00
									<span class="main-price">&pound;1259.99 <span class="vat">ex. VAT</span></span>
								</div>

								<ul class="product-attributes">
									<li>
										<span class="product-attribute-title">Size:</span>
										<span class="product-attribute-value">H:235 x W:415 x D:355 mm</span>
									</li>
									<li>
										<span class="product-attribute-title">Lock type:</span>
										<span class="product-attribute-value">Raktinė seifinė spyna</span>
									</li>
									<li>
										<span class="product-attribute-title">Cash rating:</span>
										<span class="product-attribute-value"><span class="cash-rating"><i style="width: 78%">145</i></span></span>
									</li>
									<li>
										<span class="product-attribute-title">Fire rating:</span>
										<span class="product-attribute-value"><span class="fire-rating"><i style="width: 46%">85</i></span></span>
									</li>
								</ul>

								<label class="compare-trigger">
									<input type="checkbox" /><span>Compare</span>
								</label>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="product-listing-item">
								<a href="#" class="product-main-link">
									<img src="media/prod2.png" alt="" />
									<span class="product-title">Technomax Gunsafe 45S</span>
								</a>
								<div class="product-prices">
									<span class="main-price">&pound;159.99 <span class="vat">ex. VAT</span></span>
								</div>
								<ul class="product-attributes">
									<li>
										<span class="product-attribute-title">Size:</span>
										<span class="product-attribute-value">H:235 x W:415 x D:355 mm</span>
									</li>
									<li>
										<span class="product-attribute-title">Lock type:</span>
										<span class="product-attribute-value">Raktinė seifinė spyna</span>
									</li>
									<li>
										<span class="product-attribute-title">Cash rating:</span>
										<span class="product-attribute-value"><span class="cash-rating"><i style="width: 52%"></i></span></span>
									</li>
									<li>
										<span class="product-attribute-title">Fire rating:</span>
										<span class="product-attribute-value"><span class="fire-rating"><i style="width: 64%">64</i></span></span>
									</li>
								</ul>

								<label class="compare-trigger">
									<input type="checkbox" /><span>Compare</span>
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="product-listing-item">
								<a href="#" class="product-main-link">
									<img src="media/prod3.png" alt="" />
									<span class="product-title">Technomax Technofire 30s/NGS</span>
								</a>
								<div class="product-prices">
									<span class="main-price">&pound;859.99 <span class="vat">ex. VAT</span></span>
								</div>
								<ul class="product-attributes">
									<li>
										<span class="product-attribute-title">Size:</span>
										<span class="product-attribute-value">H:235 x W:415 x D:355 mm</span>
									</li>
									<li>
										<span class="product-attribute-title">Lock type:</span>
										<span class="product-attribute-value">Elektroninė kodinė spyna</span>
									</li>
									<li>
										<span class="product-attribute-title">Cash rating:</span>
										<span class="product-attribute-value"><span class="cash-rating"><i style="width: 52%"></i></span></span>
									</li>
									<li>
										<span class="product-attribute-title">Fire rating:</span>
										<span class="product-attribute-value"><span class="fire-rating"><i style="width: 81%">64</i></span></span>
									</li>
								</ul>

								<label class="compare-trigger">
									<input type="checkbox" checked="checked" /><span>Compare</span>
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="product-listing-item product-offer-label">
								<a href="#" class="product-main-link">
									<img src="media/prod4.png" alt="" />
									<span class="product-title">Technomax 2040 D</span>
								</a>
								<div class="product-prices">
									<del>&pound;1099.99</del> Save &pound;170.00
									<span class="main-price">&pound;939.99 <span class="vat">ex. VAT</span></span>
								</div>
								<ul class="product-attributes">
									<li>
										<span class="product-attribute-title">Size:</span>
										<span class="product-attribute-value">H:235 x W:415 x D:355 mm</span>
									</li>
									<li>
										<span class="product-attribute-title">Lock type:</span>
										<span class="product-attribute-value">Raktinė kodinė spyna</span>
									</li>
									<li>
										<span class="product-attribute-title">Cash rating:</span>
										<span class="product-attribute-value"><span class="cash-rating"><i style="width: 52%"></i></span></span>
									</li>
									<li>
										<span class="product-attribute-title">Fire rating:</span>
										<span class="product-attribute-value"><span class="fire-rating"><i style="width: 64%">64</i></span></span>
									</li>
								</ul>

								<label class="compare-trigger">
									<input type="checkbox" /><span>Compare</span>
								</label>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="product-listing-item">
								<a href="#" class="product-main-link">
									<img src="media/prod5.png" alt="" />
									<span class="product-title">Technomax Mte/4</span>
								</a>
								<div class="product-prices">
									<span class="main-price">&pound;749.99 <span class="vat">ex. VAT</span></span>
								</div>
								<ul class="product-attributes">
									<li>
										<span class="product-attribute-title">Size:</span>
										<span class="product-attribute-value">H:235 x W:415 x D:355 mm</span>
									</li>
									<li>
										<span class="product-attribute-title">Lock type:</span>
										<span class="product-attribute-value">Raktinė seifinė spyna</span>
									</li>
									<li>
										<span class="product-attribute-title">Cash rating:</span>
										<span class="product-attribute-value"><span class="cash-rating"><i style="width: 52%"></i></span></span>
									</li>
									<li>
										<span class="product-attribute-title">Fire rating:</span>
										<span class="product-attribute-value"><span class="fire-rating"><i style="width: 64%">64</i></span></span>
									</li>
								</ul>

								<label class="compare-trigger">
									<input type="checkbox" checked="checked" /><span>Compare</span>
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="product-listing-item product-offer-label">
								<a href="#" class="product-main-link">
									<img src="media/prod6.png" alt="" />
									<span class="product-title">Konsmetal SG/120</span>
								</a>
								<div class="product-prices">
									<del>&pound;1539.99</del> Save &pound;220.00
									<span class="main-price">&pound;1219.99 <span class="vat">ex. VAT</span></span>
								</div>
								<ul class="product-attributes">
									<li>
										<span class="product-attribute-title">Size:</span>
										<span class="product-attribute-value">H:235 x W:415 x D:355 mm</span>
									</li>
									<li>
										<span class="product-attribute-title">Lock type:</span>
										<span class="product-attribute-value">Raktinė kodinė spyna</span>
									</li>
									<li>
										<span class="product-attribute-title">Cash rating:</span>
										<span class="product-attribute-value"><span class="cash-rating"><i style="width: 52%"></i></span></span>
									</li>
									<li>
										<span class="product-attribute-title">Fire rating:</span>
										<span class="product-attribute-value"><span class="fire-rating"><i style="width: 64%">64</i></span></span>
									</li>
								</ul>

								<label class="compare-trigger">
									<input type="checkbox" /><span>Compare</span>
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="product-listing-item">
								<a href="#" class="product-main-link">
									<img src="media/prod7.png" alt="" />
									<span class="product-title">mottura personal 11.5230</span>
								</a>
								<div class="product-prices">
									<span class="main-price">&pound;1659.99 <span class="vat">ex. VAT</span></span>
								</div>
								<ul class="product-attributes">
									<li>
										<span class="product-attribute-title">Size:</span>
										<span class="product-attribute-value">H:235 x W:415 x D:355 mm</span>
									</li>
									<li>
										<span class="product-attribute-title">Lock type:</span>
										<span class="product-attribute-value">Raktinė kodinė spyna</span>
									</li>
									<li>
										<span class="product-attribute-title">Cash rating:</span>
										<span class="product-attribute-value"><span class="cash-rating"><i style="width: 91%"></i></span></span>
									</li>
									<li>
										<span class="product-attribute-title">Fire rating:</span>
										<span class="product-attribute-value"><span class="fire-rating"><i style="width: 64%">64</i></span></span>
									</li>
								</ul>

								<label class="compare-trigger">
									<input type="checkbox" /><span>Compare</span>
								</label>
							</div>
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="pagination-holder">
	<ul class="pagination">
		<li><a href="#"><span class="caret-left"></span></a></li>
		<li><a href="#">1</a></li>
		<li><span>...</span></li>
		<li><a href="#">5</a></li>
		<li><a href="#">6</a></li>
		<li class="active"><a href="#">7</a></li>
		<li><a href="#">8</a></li>
		<li><a href="#">9</a></li>
		<li><span>...</span></li>
		<li><a href="#">14</a></li>
		<li><a href="#"><span class="caret-right"></span></a></li>
	</ul>
</div>						</div>
						
						<div class="col-xs-12 col-sm-6">
							<a href="#header" class="back-to-top">back to top</a>
						</div>
					</div>
				</div>
			</div>
			
						<div class="row visible-xs">
				<div class="col-xs-12">
					<div class="block-wizzard clearfix">
	<p class="block-wizzard-title">How to choose a safe?</p>
	<p>Choose your perfect safe in 4 easy steps</p>
	<a href="#" class="btn">Open SAFFORT Wizzard</a>
	<a href="#" class="hidden-link-hack">Open SAFFORT Wizzard</a>
	<!--We need it for link on mobile and desktop. in tablet is it hidden-->
</div>				</div>
			</div>
			
		</div>
		
		<footer id="footer">
	<div class="footer-top">
		<div class="container-fluid">
			<div class="quality-badges">
				<ul>
					<li>
						<a class="quality-badge quality-badge-icon-truck" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Free Delivery</strong>
								<span class="quality-badge-text">Free UK delivery on all orders</span>
							</span>
						</a>
					</li>
					<li>
						<a class="quality-badge quality-badge-icon-refresh" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Free Returns</strong>
								<span class="quality-badge-text">We accept returns if you are not satisfied</span>
							</span>
						</a>
					</li>
					<li>
						<a class="quality-badge quality-badge-icon-label" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Lowest price warranty</strong>
								<span class="quality-badge-text">We guarantee the best price online.</span>
							</span>
						</a>
					</li>
					<li>
						<a class="quality-badge quality-badge-icon-lock" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Safe to buy</strong>
								<span class="quality-badge-text">Be sure to stay safe online shopping with us</span>
							</span>
						</a>
					</li>
					<li>
						<a class="quality-badge quality-badge-icon-headphone" href='#'>
							<span class="quality-badge-helper">
								<strong class="quality-badge-heading">Professional advice</strong>
								<span class="quality-badge-text">Feel free to contact us with any question, we'll help you</span>
							</span>
						</a>
					</li>
				</ul>
			</div>
			<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
			<a href="#" class="jcarousel-control-next">&rsaquo;</a>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row footer-menu-blocks">
		
			<div class="col-xs-6 col-sm-6 col-lg-3 footer-menu-block">
				
				<h4>Saffort Store</h4>

				<ul class="url-list">
					<li><a href="#">About us</a></li>
					<li><a href="#">Why Saffort?</a></li>
					<li><a href="#">News and Updates</a></li>
					<li><a href="#">Contacts</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-6 col-lg-3 footer-menu-block">
				<h4>Products</h4>

				<ul class="url-list">
					<li><a href="#">View all products</a></li>
					<li><a href="#">Shop by brands</a></li>
					<li><a href="#">Shop by categories</a></li>
					<li><a href="#">Saffort Wizard</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-lg-3 footer-menu-block">

				<h4>Customer Support</h4>

				<ul class="url-list">
					<li><a href="#">Definitions A-Z</a></li>
					<li><a href="#">F.A.Q</a></li>
					<li><a href="#">Experts advice</a></li>
					<li><a href="#">Downloads</a></li>
				</ul>

			</div>
			<div class="col-xs-6 col-lg-3 footer-menu-block">
				<h4>Website Info</h4>

				<ul class="url-list">
					<li><a href="#">Payments and Delivery</a></li>
					<li><a href="#">Website Security</a></li>
					<li><a href="#">Terms and Conditions</a></li>
					<li><a href="#">Returns</a></li>
				</ul>
			</div>
		</div>

		<p>SAFFORT – tai didžiausias prekybos seifais tinklas. Mūsų tikslas – 
			sudaryti Jums galimybę vienoje vietoje rasti visus seifus. Todėl pas 
			mus parduodami: buitiniai, priešgaisriniai, sertifikuoti, šarvuoti, 
			seifai ginklams, sieniniai seifai ir t.t., kurių kokybe mes neabejojame 
			ir kuriems galime užtikrinti nepriekaištingą pristatymą ir servisą. 
			Visa tai - su geriausios kainos garantija ir nemokamu pristatymu Lietuvoje.
			Kartu siekiame, kad seifų pasirinkimo procesas būtų paprastas ir malonus.
			Išsirinkti seifą Jums nereikės specialių žinių. Kad ir koks būtų Jūsų
			norimas seifas, pas mus Jūs turite galimybę jį lengvai išsirinkti 
			pasinaudodami seifų atrankos gidu, neribotomis palyginimo galimybėmis,
			filtravimu, kurį naudojant keliais pelės paspaudimais Jūs galite iš 
			šimtų seifų surasti norimą pagal dydį, svorį, seifo spynos tipą, kainą,
			seifo gamintoją bei kitus parametrus. Išsirinktą seifą galite pamatyti 
			SAFFORT prekybos vietose – ekspozicijoje Vilniuje daugiau nei 100 seifų.
			Norime nuolatos tobulėti ir nenustebkite, kad kitą kartą apsilankę SAFFORT
			puslapyje rasite daugiau seifų, dar supaprastintą atranką ar kitų naujovių.
			Labai vertiname Jūsų pastebėjimus, tad prašome rašyti atsiliepimus apie 
			puslapį, apie parduodamus seifus, apie mūsų servisą – reaguosime ir tobulėsime.</p>
		
	</div>

	<div class="country-selector">
		<ul>
			<li><a href="#">Lithuania</a></li>
			<li><a href="#">Latvia</a></li>
			<li><a href="#">United Kingdom</a></li>
			<li><a href="#">Germany</a></li>
			<li><a href="#">France</a></li>
			<li><a href="#">Netherlands</a></li>
			<li><a href="#">Poland</a></li>
		</ul>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-lg-8 copyright">
				<img src="assets/img/logos/logo-horizontal.png" alt="" class="logo-horizontal svgFallback" />
				<p>
					&COPY; Dremler Ltd. 2013. All rights reserved. <br />
					VAT number: 742 2441 58. saffort.co.uk is a trading name of Dremler Ltd.
				</p>
			</div>
			<div class="col-xs-12 col-sm-8 col-lg-4">
				<ul class="bank-logos">
					<li><a href="#" class="bank-logo visa-logo">Visa</a></li>
					<li><a href="#" class="bank-logo master-logo">Master</a></li>
					<li><a href="#" class="bank-logo maestro-logo">Maestro</a></li>
					<li><a href="#" class="bank-logo discover-logo">Discover</a></li>
					<li><a href="#" class="bank-logo paypal-logo">Paypal</a></li>
				</ul>
			</div>
			<a href="#" class="idea-logo">IDEA Design</a>
		</div>
	</div>
</footer>

<script src="assets/js/vendor/modernizr.custom.83451.js"></script>
<script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
<script src="assets/bootstrap-3.1.1/dist/js/bootstrap.js"></script>
<script src="assets/js/vendor/jquery.jcarousel.min.js "></script>
<script src="assets/sidr-package-1.2.1/jquery.sidr.min.js"></script>
<script src="assets/js/smartresize.js"></script>
<script src="assets/js/responsive-helper.js"></script>
<script src="assets/js/badges.js"></script>
<script src="assets/js/cart-popover.js"></script>
<script src="assets/js/non-desktop-search.js"></script>
<script src="assets/js/tab-slides.js"></script>
<script src="assets/js/tab-mobile.js"></script>
<script src="assets/js/quality-badges-slider.js"></script>
<script src="assets/js/svg-fallback.js"></script>
<script src="assets/js/text-page-menu.js"></script>
<script src="assets/js/sidr.js"></script>
<script src="assets/js/compare.js"></script>
<script src="assets/js/overlay.js"></script>

<script src="assets/js/script.js"></script>

<div class="scrollbar-measure"></div>		
	</body>
</html>
