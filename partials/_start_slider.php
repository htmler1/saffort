<div id="homepage-teaser-carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#homepage-teaser-carousel" data-slide-to="0" class="active"></li>
		<li data-target="#homepage-teaser-carousel" data-slide-to="1"></li>
		<li data-target="#homepage-teaser-carousel" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner">
		<div class="item  type-slider-02 active">
			<p class="slider-title">
				Technomax Technofire 30S/NGS
			</p>
			<ul>
				<li>Electronic keypad</li>
				<li>Sertificate EN1143-1 (I class)</li>
				<li>&euro;10 000 cash rating</li>
			</ul>
			<div class="slider-product">
				<img src="media/img-slider-2.png" alt="" />
				<div class="product-prices">
					<del>&pound;1399.99</del> Save &pound;140.00
					<span class="main-price">&pound;1259.99 <span class="vat">ex. VAT</span></span>
				</div>
			</div>
		</div>
		<div class="item type-slider-03 active">
			<p class="slider-title">
				Apsaugokite savo turtą.<br />
				Šarvuoti seifai Jūsų namams.
			</p>
			<ul>
				<li>Efektyvi apsauga nuo vagysčių</li>
			</ul>
			<img src="media/slider-type3.png" alt="" />
		</div>
		<div class="item type-slider-03">
			<p class="slider-title">
				Welcome to Saffort Safes Store
			</p>
			<p>
				We offer the biggest choice of 510 safes, 14 years of experience and world class service
			</p>
		</div>
	</div>

	<!-- Controls -->
	<a class="left carousel-control" href="#homepage-teaser-carousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#homepage-teaser-carousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>