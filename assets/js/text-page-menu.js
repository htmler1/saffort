// Priklausomas nuo overlay.js
function initTextPageMenus() {
	var that = this;
	
	that.trigger = $('.block-for-text h1.submenu-dropdown-trigger');
	that.target = $('<div class="sidebar-navigation-target"/>');
	that.menu = $('.sidebar-navigation');
	that.overlay = initOverlay();
	that.open = false;
	
	that.listen = function () {
		that.trigger.on('click', function () {
			that.toggleMenu();
		});
		$(window).smartresize(function() {
			if (that.open) {
				that.toggleMenu();
			}
		});
	};
	
	that.toggleMenu = function () {
		if (that.open) {
			that.target.hide();
			that.trigger.removeClass('active');
			that.overlay.removeClass('active');
			that.open = false;
		} else if (!isDesktop()) {
			that.target.show();
			that.target.outerWidth(that.trigger.outerWidth());
			that.trigger.addClass('active');
			that.overlay.addClass('active');
			that.open = true;
		}
	};
	
	that.init = function () {
		if (that.trigger.length > 0) {
			that.target.html(that.menu[0].outerHTML).hide();
			that.trigger.after(that.target);
			that.listen();
		}
	};
	
	that.init();
}