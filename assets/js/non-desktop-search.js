/**
 * Checks or we are not in desktop mode.
 * If no, then open text field.
 * If text field already opened, submit form
 */
function nonDesktopSearch () {
	
	$('#header .search-form button').click(function () {
		if (!isDesktop()) {
			var searchForm = $(this).parent('.search-form');
			if (!searchForm.hasClass('open')) {
				searchForm.addClass('open');
				return false;
			}
		}
	});
	
	// allways closes search field
	$('#header .search-form .close').click(function () {
		var searchForm = $(this).parent('.search-form');
		searchForm.removeClass('open');
	});
	
}