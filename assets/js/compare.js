function initCompareSticker() {
	$('.compare-sticky').on('click', function () {
		$(this).toggleClass('open');
		$('.compare-list').toggleClass('open');
	});
	
	// on resize - close
	$(window).smartresize(function() {
		$('.compare-sticky').removeClass('open');
		$('.compare-list').removeClass('open');
	});
}