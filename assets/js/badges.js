var BadgesSlider = function (timeout) {
	
	var $badges = $('.badges-slider .slider-item').addClass('hidden'),
		current = $badges.first().addClass('animated fadeIn').removeClass('hidden'),
		that = this;
	
	this.slide = function () {
		if ($badges.length < 2) {
			return;
		}
		var next = current.next();
		if (next.length === 0) {
			next = $badges.first();
		}
		current.addClass('animated fadeOut');
		current.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
			current.addClass('hidden').removeClass('animated fadeOut');
			next.addClass('animated fadeIn').removeClass('hidden');
			current = next;
			that.setTimeout();
		});
	};
	
	this.setTimeout = function () {
		setTimeout(that.slide, timeout);
	};
	
	that.setTimeout();
	
};