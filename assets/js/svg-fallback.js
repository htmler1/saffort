/**
 * Pictures width class .svgFallback in SVG supported browsers are like SVG loaded
 * @todo it would better, if would all pictures already in HTML as SVG, because
 *	then we dont need load PNG additionally
 * 
 * http://toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script/
 */
function svgFallback () {
	if (Modernizr.svg) {
		$('.svgFallback').each(function() {
//			$('img[src*="svg"]')
			$(this).attr('src', function() {
				return $(this).attr('src').replace('.png', '.svg');
			});
		});
	}
}